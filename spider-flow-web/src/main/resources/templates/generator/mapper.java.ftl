package ${package.Mapper};

import ${package.Entity}.${entity};
<#-- import ${superMapperClassPackage};-->
<#if mapperAnnotation>
import org.apache.ibatis.annotations.Mapper;
</#if>
import com.example.dldemo.config.base.SuperMapper;

/**
 * @author ${author} ${table.comment!} Mapper 接口
 * @since ${date}
 */
<#if mapperAnnotation>
@Mapper
</#if>
<#if kotlin>
interface ${table.mapperName} : ${superMapperClass}<${entity}>
<#else>
public interface ${table.mapperName} extends ${superMapperClass}<${entity}> {

}
</#if>
