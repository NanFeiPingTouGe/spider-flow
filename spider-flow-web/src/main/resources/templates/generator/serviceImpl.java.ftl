package ${package.ServiceImpl};

import ${package.Entity}.${entity};
import ${package.Mapper}.${table.mapperName};
import ${package.Service}.${table.serviceName};
import ${superServiceImplClassPackage};
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import cn.hutool.core.convert.Convert;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.dldemo.utils.SqlUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;
import java.util.List;
import java.util.Set;

import static com.example.dldemo.utils.constant.Constant.*;

/**
 * @author ${author} ${table.comment!}
 * @since ${date}
 */
@Slf4j
@Service
@RequiredArgsConstructor
<#if kotlin>
open class ${table.serviceImplName} : ${superServiceImplClass}<${table.mapperName}, ${entity}>(), ${table.serviceName} {

}
<#else>
public class ${table.serviceImplName} extends ${superServiceImplClass}<${table.mapperName}, ${entity}> implements ${table.serviceName} {

    @Override
    public Page<${entity}> get${entity}Page(Map<String, Object> params) {
        return super.getPage(params);
    }

    @Override
    public List<${entity}> get${entity}List(Map<String, Object> params) {
        QueryWrapper<${entity}> queryWrapper = super.getQueryWrapper(params, true,CREATE_TIME);
        return baseDao.selectList(queryWrapper);
    }

    @Override
    public Boolean insert${entity}(${entity} ${entity?uncap_first}) {
        return baseDao.insert(${entity?uncap_first}) > 0;
    }

    @Override
    public Boolean delete${entity}BatchIds(Set<Long> ids) {
        return baseDao.deleteBatchIds(ids) > 0;
    }

    @Override
    public Boolean update${entity}ById(${entity} ${entity?uncap_first}) {
        return baseDao.updateById(${entity?uncap_first}) > 0;
    }

}
</#if>
