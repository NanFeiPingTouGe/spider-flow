package ${package.Service};

import ${package.Entity}.${entity};
import ${superServiceClassPackage};
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.transaction.annotation.Transactional;
import java.util.Map;
import java.util.List;
import java.util.Set;

/**
 * @author ${author} ${table.comment!}
 * @since ${date}
 */
<#if kotlin>
interface ${table.serviceName} : ${superServiceClass}<${entity}>
<#else>
public interface ${table.serviceName} extends ${superServiceClass}<${entity}> {

    Page<${entity}> get${entity}Page(Map<String, Object> params);

    List<${entity}> get${entity}List(Map<String, Object> params);

    @Transactional(rollbackFor = Exception.class)
    Boolean delete${entity}BatchIds(Set<Long> params);

    @Transactional(rollbackFor = Exception.class)
    Boolean insert${entity}(${entity} ${entity?uncap_first});

    @Transactional(rollbackFor = Exception.class)
    Boolean update${entity}ById(${entity} ${entity?uncap_first});

}
</#if>
