package ${package.Controller};

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;
import com.example.dldemo.annotation.log.Log;
import org.springframework.web.bind.annotation.RequestMapping;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ${package.Service}.${table.serviceName};
import ${package.Entity}.${entity};
import springfox.documentation.annotations.ApiIgnore;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import cn.hutool.core.lang.Assert;
<#if restControllerStyle>
import org.springframework.web.bind.annotation.RestController;
<#else>
import org.springframework.stereotype.Controller;
</#if>
<#if superControllerClassPackage??>
import ${superControllerClassPackage};
</#if>
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.dldemo.utils.Result;

import java.util.Map;
import java.util.List;
import java.util.Set;

import static com.example.dldemo.utils.constant.Constant.*;

/**
 * @author ${author} ${table.comment!}
 * @since ${date}
 */
@Slf4j
<#if restControllerStyle>
@RestController
<#else>
@Controller
</#if>
@RequestMapping("/<#if controllerMappingHyphenStyle>${controllerMappingHyphen}<#else>${table.entityPath}</#if>")
<#if kotlin>
class ${table.controllerName}<#if superControllerClass??> : ${superControllerClass}()</#if>
<#else>
@RequiredArgsConstructor
@Api(tags = "${table.comment!}控制器")
<#if superControllerClass??>
public class ${table.controllerName} extends ${superControllerClass} {
<#else>
public class ${table.controllerName} {
</#if>

    private final ${table.serviceName} ${table.serviceName?uncap_first};

    @ApiOperation(value = "${table.comment!}控制器-分页查询")
    @GetMapping("/get${entity}Page")
    @ApiImplicitParams({
        @ApiImplicitParam(required = true, name = PAGE_NUM, value = PAGE_NUM_TEXT, dataType = "String", paramType = "query"),
        @ApiImplicitParam(required = true, name = PAGE_SIZE, value = PAGE_SIZE_TEXT, dataType = "String", paramType = "query"),
    })
    public Result<Page<${entity}>> get${entity}Page(@ApiIgnore @RequestParam Map<String, Object> params) {
        return Result.success(${entity?uncap_first}Service.get${entity}Page(params));
    }

    @ApiOperation(value = "${table.comment!}控制器-查询全部")
    @GetMapping("/get${entity}List")
    public Result<List<${entity}>> getUserList(@ApiIgnore @RequestParam Map<String, Object> params) {
        return Result.success(${entity?uncap_first}Service.get${entity}List(params));
    }

    @Log("${table.comment!}控制器-新增")
    @ApiOperation(value = "新增")
    @PostMapping("/insert${entity}")
    public Result<Boolean> insert${entity}(@Validated @RequestBody ${entity} ${entity?uncap_first}) {
        Assert.isnull(task.getId(), "id应为空");
        return Result.success(${entity?uncap_first}Service.insert${entity}(${entity?uncap_first}));
    }

    @Log("${table.comment!}控制器-通过Id删除多条")
    @ApiOperation(value = "通过Id删除多条")
    @DeleteMapping("/delete${entity}BatchIds")
    public Result<Boolean> delete${entity}BatchIds(@RequestBody Set<Long> ids) {
        return Result.success(${entity?uncap_first}Service.delete${entity}BatchIds(ids));
    }

    @Log("${table.comment!}控制器-修改")
    @ApiOperation(value = "修改")
    @PutMapping("/update${entity}ById")
    public Result<Boolean> update${entity}ById(@Validated @RequestBody ${entity} ${entity?uncap_first}) {
        Assert.notNull(task.getId(), "id不应为空");
        return Result.success(${entity?uncap_first}Service.update${entity}ById(${entity?uncap_first}));
    }

}
</#if>
