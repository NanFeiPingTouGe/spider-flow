package org.spiderflow.bean;

import org.spiderflow.core.utils.ProxyProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @apiNote 配置文件转换Pojo类的 统一配置 类
 * @author: Dc
 * @date: 2020/6/10 19:04
 */
@Configuration
public class ConfigBeanConfiguration {

    @Bean
    @ConfigurationProperties(prefix = "spider.proxy")
    public ProxyProperties loginProperties() {
        return new ProxyProperties();
    }

}
