package org.spiderflow.mybatisPlus;

import cn.hutool.extra.spring.SpringUtil;
import org.apache.ibatis.cache.Cache;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * @author Dc 😁
 * @description: myvatis redis 二级缓存
 * @date 2022/6/28 9:09
 */
public class MybatisRedisCache implements Cache {

    private final RedisTemplate<Object, Object> redisTemplate = SpringUtil.getBean("redisTemplate");
    private final String id;

    public MybatisRedisCache(String id) {
        this.id = id;
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public void putObject(Object key, Object value) {
        redisTemplate.opsForHash().put(id, key.toString(), value);
    }

    @Override
    public Object getObject(Object key) {
        return redisTemplate.opsForHash().get(id, key.toString());
    }

    @Override
    public Object removeObject(Object o) {
        return null;
    }

    /**
     * @description: 当发送增删改操作时clear方法被调用
     * @author Dc 😁
     * @date 2022/6/28 9:11
     */
    @Override
    public void clear() {
        redisTemplate.delete(id);
    }

    /**
     * @description: 缓存中有多少条缓存
     * @author Dc 😁
     * @date 2022/6/28 9:11
     */
    @Override
    public int getSize() {
        return redisTemplate.opsForHash().size(id).intValue();
    }
}