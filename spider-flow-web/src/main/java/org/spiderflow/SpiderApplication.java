package org.spiderflow;

import cn.hutool.extra.spring.EnableSpringUtil;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import java.io.IOException;

@SpringBootApplication
@EnableScheduling
@MapperScan("org.spiderflow.*.mapper")
@EnableSpringUtil
public class SpiderApplication implements ServletContextInitializer{
	
	public static void main(String[] args) throws IOException {
		
		SpringApplication.run(SpiderApplication.class, args);

		System.out.println("\n------ 启动成功 ------");
//		System.out.println("name: " + SaQuickManager.getConfig().getName());
//		System.out.println("pwd:  " + SaQuickManager.getConfig().getPwd());

	}

	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		//设置文本缓存1M
		servletContext.setInitParameter("org.apache.tomcat.websocket.textBufferSize", Integer.toString((1024 * 1024)));
	}
	

}
