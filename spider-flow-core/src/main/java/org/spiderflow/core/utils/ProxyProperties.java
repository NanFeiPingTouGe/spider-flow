package org.spiderflow.core.utils;

import lombok.Data;

@Data
public class ProxyProperties {
    private String host;
    private Integer port;
    private String addr;
    private String delAddr;
    private String countAddr;
}
