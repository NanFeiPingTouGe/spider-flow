package org.spiderflow.core.executor.function;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.spiderflow.annotation.Comment;
import org.spiderflow.annotation.Example;
import org.spiderflow.core.utils.ProxyProperties;
import org.spiderflow.executor.FunctionExecutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 字符串内容和Base64互相转换 工具类 防止NPE
 *
 * @author Administrator
 */
@Component
@Comment("Ip代理常用方法")
@RequiredArgsConstructor
@Slf4j
public class ProxyFunctionExecutor implements FunctionExecutor {

    private static ProxyProperties proxyProperties = null;

    @Override
    public String getFunctionPrefix() {
        return "proxy";
    }


    @Autowired
    public void setProxyProperties(ProxyProperties proxyProperties) {
        ProxyFunctionExecutor.proxyProperties = proxyProperties;
    }

    @Comment("得到一个代理IP")
    @Example("${proxy.getProxy()}")
    public static String getProxy(String type) throws InterruptedException {
        final String url = StrUtil.format("http://{}:{}/{}/?type={}", proxyProperties.getHost(), proxyProperties.getPort(), proxyProperties.getAddr(),type);
        return JSONUtil.parseObj(httpCreate(url)).getStr("proxy");
    }

    private static Object httpCreate(String url) throws InterruptedException {
        HttpResponse execute = HttpUtil.createGet(url).execute();
        if (!execute.isOk()) {
            throw new InterruptedException(StrUtil.format("execute code is {}", execute.getStatus()));
        }
        String body = execute.body();
        JSONObject entries = JSONUtil.parseObj(body);
        return entries;
    }

    @Comment("删除一个代理IP")
    @Example("${proxy.delProxy()}")
    public static void delProxy(Object proxy) throws InterruptedException {
        final String delUrl = StrUtil.format("http://{}:{}/{}/?proxy={}", proxyProperties.getHost(), proxyProperties.getPort(), proxyProperties.getDelAddr(), proxy.toString());
        final String countUrl = StrUtil.format("http://{}:{}/{}/", proxyProperties.getHost(), proxyProperties.getPort(), proxyProperties.getCountAddr());
        JSONUtil.parseObj(httpCreate(delUrl));
        log.info("删除代理成功---->{}", proxy);
        JSONObject http_type = JSONUtil.parseObj(httpCreate(countUrl)).getJSONObject("http_type");
        log.info("剩余代理数---->{}", http_type.toString());
    }

    @Comment("得到ID")
    @Example("${proxy.getId()}")
    public static Long getId() {
        return IdWorker.getId();
    }

}
