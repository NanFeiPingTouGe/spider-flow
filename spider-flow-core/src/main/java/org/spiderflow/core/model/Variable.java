package org.spiderflow.core.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@TableName("sp_variable")
@Data
public class Variable {

	@TableId(type = IdType.AUTO)
	private Integer id;

	private String name;

	private String value;

	private String description;

	private Date createDate;
}
