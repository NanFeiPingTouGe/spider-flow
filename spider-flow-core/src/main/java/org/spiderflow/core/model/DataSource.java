package org.spiderflow.core.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@TableName("sp_datasource")
@Data
public class DataSource {

    @TableId(type = IdType.ASSIGN_UUID)
    private String id;

    private String name;

    private String driverClassName;

    private String jdbcUrl;

    private String username;

    private String password;

    private Date createDate;
}
