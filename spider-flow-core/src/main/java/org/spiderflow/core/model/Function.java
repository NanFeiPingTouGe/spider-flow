package org.spiderflow.core.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@TableName("sp_function")
@Data
public class Function {

    @TableId(type = IdType.ASSIGN_UUID)
    private String id;

    private String name;

    private String parameter;

    private String script;

    private Date createDate;
}
