package org.spiderflow.core.mapper.base;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 演示 mapper 父类，注意这个类不要让 mp 扫描到！！
 *
 * @author Dc 😁
 */
public interface SuperMapper<T> extends BaseMapper<T> {

}
