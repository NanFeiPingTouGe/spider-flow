package org.spiderflow.core.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.spiderflow.core.mapper.base.SuperMapper;
import org.spiderflow.core.model.Task;

@Mapper
public interface TaskMapper extends SuperMapper<Task> {
}
