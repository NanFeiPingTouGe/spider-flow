package org.spiderflow.core.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.spiderflow.core.mapper.base.SuperMapper;
import org.spiderflow.core.model.Function;

@Mapper
public interface FunctionMapper extends SuperMapper<Function> {

}
