package org.spiderflow.core.mapper;

import org.spiderflow.core.mapper.base.SuperMapper;
import org.spiderflow.core.model.Variable;

public interface VariableMapper extends SuperMapper<Variable> {
}
