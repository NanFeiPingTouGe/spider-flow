package org.spiderflow.core.mapper.base;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author ducheng
 */
public class SuperServiceImpl<M extends SuperMapper<T>, T> extends ServiceImpl<M, T> {

    @Autowired
    protected M baseDao;

//    public QueryWrapper<T> getQueryWrapper(Map<String, Object> params, boolean isAsc, String... order) {
//        ArrayList<String> strings = ListUtil.toList(order);
//        if (strings.size() < 1) {
//            strings.add(CREATE_TIME);
//        }
//        QueryWrapper<T> queryWrapper = new QueryWrapper<T>()
//                .orderBy(order.length > 0, isAsc, strings);
//        Type genType = getClass().getGenericSuperclass();
//        Type[] types = ((ParameterizedType) genType).getActualTypeArguments();
//        Field[] declaredFields = ((Class) types[1]).getDeclaredFields();
//        for (final Field declaredField : declaredFields) {
//            String name = declaredField.getName();
//            TableField annotation = declaredField.getAnnotation(TableField.class);
//            final boolean notEmpty = ObjectUtil.isNotEmpty(params.get(name));
//            final String s = StrUtil.toSymbolCase(name, '_');
//            final boolean notEmpty1 = ObjectUtil.isNotEmpty(params.get(s));
//            boolean hasColumn = (notEmpty || notEmpty1) && (null == annotation || annotation.exist());
//            if (hasColumn) {
//                queryWrapper.eq(StrUtil.toSymbolCase(name, '_'), notEmpty ? params.get(name) : params.get(s));
//            }
//        }
//        return queryWrapper;
//    }
//
//    /**
//     * 获取分页对象
//     */
//    public Page<T> getPage(Map<String, Object> params) {
//        Integer pageNum = Convert.toInt(params.get(PAGE_NUM));
//        Integer pageSize = Convert.toInt(params.get(PAGE_SIZE));
//        if (ObjectUtil.hasEmpty(pageNum, pageSize)) {
//            throw new BadRequestException("{} or {} not exists", PAGE_NUM, PAGE_SIZE);
//        }
//        return new Page<T>(pageNum, pageSize);
//    }
//
//    public Page<T> selectPageByQW(Map<String, Object> params, boolean isAsc, String... order) {
//        return baseDao.selectPage(
//                getPage(params),
//                getQueryWrapper(params, isAsc, order)
//        );
//    }

}
