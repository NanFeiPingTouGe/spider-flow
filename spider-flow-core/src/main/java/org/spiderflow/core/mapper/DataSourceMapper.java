package org.spiderflow.core.mapper;

import org.apache.ibatis.annotations.Select;
import org.spiderflow.core.mapper.base.SuperMapper;
import org.spiderflow.core.model.DataSource;

import java.util.List;

public interface DataSourceMapper extends SuperMapper<DataSource> {
	
	@Select("select id,name from sp_datasource")
	List<DataSource> selectAll();

}
