/**
 * Copyright (c) 2018 人人开源 All rights reserved.
 * <p>
 * https://www.renren.io
 * <p>
 * 版权所有，侵权必究！
 */

package org.spiderflow.core.mapper.base;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

/**
 * 基础实体类，所有实体都需要继承
 *
 * @author Mark sunlightcs@gmail.com
 */
@Data
public abstract class SuperEntity<T extends Model<?>> extends Model<T> {

}