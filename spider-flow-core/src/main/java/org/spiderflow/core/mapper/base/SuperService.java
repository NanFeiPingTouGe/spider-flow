package org.spiderflow.core.mapper.base;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author ducheng
 */
public interface SuperService<T> extends IService<T> {

}
