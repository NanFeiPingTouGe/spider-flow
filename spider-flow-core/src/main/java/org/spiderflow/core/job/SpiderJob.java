package org.spiderflow.core.job;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.spiderflow.context.SpiderContext;
import org.spiderflow.context.SpiderContextHolder;
import org.spiderflow.core.Spider;
import org.spiderflow.core.model.SpiderFlow;
import org.spiderflow.core.model.Task;
import org.spiderflow.core.service.SpiderFlowService;
import org.spiderflow.core.service.TaskService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 爬虫定时执行
 *
 * @author Administrator
 */
@Component
@RequiredArgsConstructor
@Slf4j
public class SpiderJob extends QuartzJobBean {

    private final Spider spider;

    @Resource
    @Lazy
    private SpiderFlowService spiderFlowService;

    private final TaskService taskService;
    private final static Map<Integer, SpiderContext> contextMap = new HashMap<>();

    @Value("${spider.job.enable:true}")
    private boolean spiderJobEnable;

    @Value("${spider.workspace}")
    private String workspace;

    @Override
    protected void executeInternal(JobExecutionContext context) {
        if (!spiderJobEnable) {
            return;
        }
        JobDataMap dataMap = context.getMergedJobDataMap();
        SpiderFlow spiderFlow = (SpiderFlow) dataMap.get(SpiderJobManager.JOB_PARAM_NAME);
        if ("1".equalsIgnoreCase(spiderFlow.getEnabled())) {
            run(spiderFlow, context.getNextFireTime());
        }
    }

    public void run(String id) {
        run(spiderFlowService.getById(id), null);
    }

    public void run(SpiderFlow spiderFlow, Date nextExecuteTime) {
        Task task = new Task();
        task.setFlowId(spiderFlow.getId());
        task.setBeginTime(new Date());
        taskService.save(task);
        run(spiderFlow, task, nextExecuteTime);
    }

    public void run(SpiderFlow spiderFlow, Task task, Date nextExecuteTime) {
        SpiderJobContext context = null;
        Date now = new Date();
        try {
            context = SpiderJobContext.create(this.workspace, spiderFlow.getId(), task.getId(), false);
            SpiderContextHolder.set(context);
            contextMap.put(task.getId(), context);
            log.info("开始执行任务{}", spiderFlow.getName());
            spider.run(spiderFlow, context);
            log.info("执行任务{}完毕，下次执行时间：{}", spiderFlow.getName(), nextExecuteTime == null ? null : DateFormatUtils.format(nextExecuteTime, "yyyy-MM-dd HH:mm:ss"));
        } catch (Exception e) {
            log.error("执行任务{}出错", spiderFlow.getName(), e);
        } finally {
            if (context != null) {
                context.close();
            }
            task.setEndTime(new Date());
            taskService.saveOrUpdate(task);
            contextMap.remove(task.getId());
            SpiderContextHolder.remove();
        }
        spiderFlowService.executeCountIncrement(spiderFlow.getId(), now, nextExecuteTime);
    }

    public static SpiderContext getSpiderContext(Integer taskId) {
        return contextMap.get(taskId);
    }
}
